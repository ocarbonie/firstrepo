#!/bin/bash

echo " " > serverinfo.info

echo "Today's Date:"  >> serverinfo.info
date +%D  >> serverinfo.info          

echo " " >> serverinfo.info

echo "Last 10 users to log in:"i >>serverinfo.info
lastlog > tempfile.txt
tail -n 10 tempfile.txt | awk '{print $1} ' >> serverinfo.info

echo " " >> serverinfo.info

echo "Swap Space:" >> serverinfo.info
free -m > tempfile.txt
head -n 1 tempfile.txt > tempfile2.txt
tail -n 1 tempfile.txt >>tempfile2.txt
cat tempfile2.txt >> serverinfo.info

rm -f tempfile.txt tempfile2.txt

echo " " >> serverinfo.info

echo "Kernel Version:"  >> serverinfo.info
uname -r >> serverinfo.info

echo " "  >> serverinfo.info

echo "IP address: "  >> serverinfo.info
hostname -i | grep -E -o "([0-9]{1,3}\.){3}[0-9]{1,3}" >> serverinfo.info

echo " " >> serverinfo.info


